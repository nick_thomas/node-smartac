# Theorem Practical Exercise - Proof of Concept for SmartAC

Please read these documents to begin the project:

* [Introduction](docs/project/smartac-intro.md)
* [SmartAC specification](docs/project/smartac-spec.md)
* Prioritization:
  * for [Backend-only variation](docs/project/smartac-priorities-backend-only.md)
  * for [Fullstack variation](docs/project/smartac-priorities-fullstack.md)
* [Wireframes](https://www.figma.com/file/uYhDDrxN5wq7r837wO8A7Q/Wireframes) for reference

Add any additional project documentation here as you work on the project and complete the assigned tasks.
