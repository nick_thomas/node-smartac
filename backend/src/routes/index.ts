import express, { Application } from "express";
import DeviceRouter from "./DeviceRouter";
import AuthRouter from "./AuthRouter";
import DeviceReadingRouter from "./DeviceReadingRouter";
export function initRoutes(app: Application): void {
  const router = express.Router();
  app.use("/v1/device", DeviceRouter);
  app.use("/v1/devicereading", DeviceReadingRouter);
  app.use("/v1/auth", AuthRouter);
}
