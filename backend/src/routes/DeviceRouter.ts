import { Router } from "express";
import * as DeviceController from "./../controllers/Device/index";
import { validate, Joi } from "express-validation";
const DeviceRouter = Router();

/**
 * Details for registering a device with secret key
 * @typedef {object} Register
 * @property {string} serialNumber.required
 * @property {string} sharedKey.required
 */

/**
 * POST /v1/device/register
 * @summary This is the summary of the endpoint
 * @tags BE-DEV-1
 * @param {Register} request.body.required - register details
 * @return {object} 200 - success response - application/json
 * @return {object} 400 - Bad request response
 */
DeviceRouter.post("/register", DeviceController.createMock);

export default DeviceRouter;
