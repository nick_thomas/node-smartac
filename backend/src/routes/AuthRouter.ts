import { Router } from "express";
import * as AuthComponent from "./../controllers/Auth/index";
import { validate, Joi } from "express-validation";
const router = Router();

router.post("/register", AuthComponent.register);

export default router;
