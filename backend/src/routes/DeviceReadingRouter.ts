import { Router } from "express";
import { tokenValidate } from "../controllers/Auth";
const DeviceReadingRouter = Router();

/**
 * Details for sending a snapshot
 * @typedef {object} snapshot
 * @property {string} serialNumber.required
 * @property {string} token.required
 */

/**
 * POST v1/devicereading/snapshot
 * @summary this is the summary of the endpoint
 * @tags BE-DEV-2
 * @param {Register} request.body.required - register details
 * @return {object} 200 - success response application/json
 * @return {object} 400 - Bad Request
 */
DeviceReadingRouter.post("/snapshot", tokenValidate);

export default DeviceReadingRouter;
