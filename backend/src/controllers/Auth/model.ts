import { IDevice } from "../Device/model";

export type ValidateSerialToken = Pick<IDevice, "token" | "serialNumber">;
