import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { IDevice } from "../Device/model";
import AuthService from "./service";

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @param {Pick<IDevice,"token">} token
 * @param {string} resMessage
 */
export function tokenValidate(
  req: Request,
  res: Response,
  next: NextFunction
): void {
  const { token, serialNumber }: Pick<IDevice, "token" | "serialNumber"> =
    req.body;
  const tokenSigned = jwt.verify(token, "secret") as
    | jwt.JwtPayload
    | Pick<IDevice, "serialNumber">;
  if (tokenSigned.serialNumber === serialNumber) {
    console.log("Super duper");
    res.json({
      text: "woohoo",
    });
    //    next();
  } else {
    throw new Error("Unauthenticated");
  }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns {Promise <void>}
 */
export async function register(req: Request, res: Response): Promise<void> {
  try {
    const device: IDevice = await AuthService.registerDevice(req.body);
    res.json({
      status: 200,
      device,
    });
  } catch (error: unknown) {
    if (error instanceof Error) {
      res.json({
        status: 400,
        message: error.message,
      });
    }
  }
}
