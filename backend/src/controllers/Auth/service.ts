import Joi from "joi";
import DeviceModel, { IDevice } from "../Device/model";
import { IAuthService } from "./interface";
import AuthValidation from "./validation";
import * as jwt from "jsonwebtoken";

/**
 * @export
 * @implements {IAuthService}
 */
const AuthService: IAuthService = {
  /**
   * @param {IDevice} body
   * @returns {Promise<IDevice>}
   * @member of AuthService
   */

  async registerDevice(data: IDevice): Promise<IDevice> {
    try {
      const validate: Joi.ValidationResult =
        AuthValidation.registerDevice(data);
      if (validate.error) {
        throw new Error(validate.error.message);
      }
      const { serialNumber, sharedKey } = data;

      const token = jwt.sign({ serialNumber }, "secret", {
        expiresIn: "60m",
      });

      const query: IDevice | null = await DeviceModel.findOneAndUpdate(
        {
          serialNumber,
          sharedKey,
        },
        { token }
      );
      if (!query) {
        throw new Error("No device found matching that");
      }
      let { _id, ...response } = query.toObject();
      if (response.hasOwnProperty("token")) {
        return response as IDevice;
      }
      return { ...response, token } as IDevice;
    } catch (error) {
      if (!(error instanceof Error)) {
        throw new Error("error object missing");
      }
      throw new Error(error.message);
    }
  },
};

export default AuthService;
