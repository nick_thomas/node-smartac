import { IDevice } from "../Device/model";

/**
 * @export
 * @interface IAuthService
 */
export interface IAuthService {
  /**
   * @param {IDevice}
   * @returns {Promise<IDevice>}
   * @memberof AuthService
   */
  registerDevice(data: IDevice): Promise<IDevice>;
}
