import Joi from "joi";
import Validation from "../validation";
import { IDevice } from "../Device/model";
/**gT
 * @export
 * @class AuthValidation
 * @extends Validation
 */
class AuthValidation extends Validation {
  /**
   * Creates an instance of AuthValidation
   * @memberof AuthValidation
   */
  constructor() {
    super();
  }

  /**
   * @param {IDevice}
   * @returns {Joi.ValidationResult}
   * @memberof DeviceValidation
   */
  registerDevice(params: IDevice) {
    const schema: Joi.Schema = Joi.object().keys({
      serialNumber: Joi.string().required(),
      sharedKey: Joi.string().required(),
    });

    return schema.validate(params);
  }
}

export default new AuthValidation();
