import * as Joi from "joi";
import DeviceValidation from "./validation";
import { SerialAndKey } from "./interface";
import DeviceModel, { IDevice } from "./model";

/**
 * @export
 *
 */
export const DeviceService = {
  /**
   * @returns {Promise<IDevice>}
   * @memberof UserService
   */
  async findBySerialAndKey(data: SerialAndKey): Promise<IDevice> {
    try {
      const validate: Joi.ValidationResult =
        DeviceValidation.registerDevice(data);
      if (validate.error) {
        throw new Error(validate.error.message);
      }
      const { serialNumber, sharedKey } = data;
      const device = await DeviceModel.findOne({
        serialNumber,
        sharedKey,
      }).exec();
      if (device === null) {
        throw new Error("No device found matching that");
      }
      return device;
    } catch (error) {
      if (!(error instanceof Error)) {
        throw new Error("error object missing");
      }
      throw new Error(error.message);
    }
  },
};
