import Joi from "joi";
import Validation from "../validation";
import { SerialAndKey } from "./interface";

/**
 * @export
 * @class DeviceValidation
 * @extends Validation
 */
class DeviceValidation extends Validation {
  /**
   * Creates an instance of DeviceValidation
   * @memberof DeviceValidation
   */
  constructor() {
    super();
  }

  /**
   * @param {IDevice}
   * @returns {Joi.ValidationResult}
   * @memberof DeviceValidation
   */
  registerDevice(params: SerialAndKey) {
    const schema: Joi.Schema = Joi.object().keys({
      serialNumber: Joi.string().required(),
      sharedKey: Joi.string().required(),
    });

    return schema.validate(params);
  }
}

export default new DeviceValidation();
