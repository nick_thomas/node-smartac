import { Request, Response } from "express";
import { SerialAndKey } from "./interface";
import { IDevice } from "./model";
import { DeviceService } from "./service";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns {Promise<void>}
 */
export async function createMock(req: Request, res: Response): Promise<void> {
  try {
    const device: Pick<IDevice, "serialNumber" | "firmwareVersion"> =
      await DeviceService.findBySerialAndKey(req.body as SerialAndKey);
    if (device === null) {
      throw new Error("Device is not found");
    }
    const authToken = jwt.sign(
      {
        id: device.serialNumber,
      },
      process.env.JWT_SECRET!,
      {
        expiresIn: process.env.JWT_EXPIRY,
      }
    );
    const response = {
      firmwareVersion: device.firmwareVersion,
      authToken,
    };
    res.status(201).json(response);
  } catch (error) {
    if (error instanceof Error) {
      res.status(400).send({ error: error.message });
    }
  }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns {Promise<void>}
 */
export async function reportReadings(req: Request, res: Response) {}
