import { Document, Schema } from "mongoose";
import { configuredDatabase } from "../../config/connections/connections";

/**
 * @export
 * @interface IDevice
 * @extends {Document}
 */
export interface IDevice extends Document {
  serialNumber: string;
  registrationDate: Date;
  recentRegistrationDate: Date;
  sharedKey: string;
  firmwareVersion: string;
  token: string;
}

export type AuthToken = {
  accessToken: string;
  kind: string;
};

const DeviceSchema: Schema = new Schema(
  {
    serialNumber: {
      type: String,
      unique: true,
      trim: true,
    },
    registrationDate: Date,
    sharedKey: {
      type: String,
      unique: true,
      trim: true,
    },
    recentRegistrationDate: Date,
    firmwareVersion: String,
    token: String,
  },
  {
    collection: "device",
  }
);

const DeviceModel = configuredDatabase.model<IDevice>(
  "DeviceModel",
  DeviceSchema
);

export default DeviceModel;
