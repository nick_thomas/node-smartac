export type SerialAndKey = {
  sharedKey: string;
  serialNumber: string;
};
