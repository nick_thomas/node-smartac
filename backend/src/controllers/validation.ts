import * as Joi from "joi";
import { Types } from "mongoose";

/**
 * @export
 * @class Validation
 */
abstract class Validation {
  customJoi: unknown;

  /**
   * @static
   * @type
   * @memberof JoiScheme
   */
  readonly messageObjectId: string =
    "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";

  /**
   * Creates an instance of Schema
   * @memberof JoiSchema
   */
  constructor() {
    this.customJoi = Joi.extend((joi) => ({
      type: "objectId",
      base: joi.string(),
      validate(value: any, helpers: Joi.CustomHelpers): Object | string {
        if (!Types.ObjectId.isValid(value)) {
          return (this as any).createError(
            "objectId.base",
            {
              value,
            },
            helpers
          );
        }
        return value;
      },
    }));
  }
}

export default Validation;
