import Joi from "joi";
import DeviceReadingModel, { IDeviceReadings } from "./model";
import { IDeviceReadingSerivce } from "./interface";
import DeviceReadingValidation from "./validation";

/**
 * @export
 * @implements {IDeviceReadingSerivce}
 */

const DeviceReadingService: IDeviceReadingSerivce = {
  /**
   * @param {IDeviceReadings}
   * @returns {Promise<void>}
   */
  async createSnapshot(data: IDeviceReadings): Promise<void> {
    try {
      const validate: Joi.ValidationResult =
        DeviceReadingValidation.validateReading(data);
      if (validate.error) {
        throw new Error(validate.error.message);
      }
      const query: IDeviceReadings = new DeviceReadingModel({
        ...data,
      });
      await query.save();
      // where should it be returned
      return;
    } catch (error) {
      if (!(error instanceof Error)) {
        throw new Error("error object missing");
      }
      throw new Error(error.message);
    }
  },
};

export default DeviceReadingService;
