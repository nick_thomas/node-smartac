import { IDeviceReadings } from "./model";
import DeviceReadingService from "./service";

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns {Promise<void>}
 */

// Todo what should be the data to be sent, as of now
// you need to send serial key, token, and three values (carbon,humidity & temperature)
// You must set the health status on the backend along with the registration time

export async function createSnapshot(
  req: Request,
  res: Response
): Promise<void> {
  try {
    const readings: IDeviceReadings = await DeviceReadingService.createSnapshot(
      req.body
    );
  } catch (error: unknown) {
    if (error instanceof Error) {
      res.json({
        status: 401,
        message: error.message,
      });
    }
  }
}
