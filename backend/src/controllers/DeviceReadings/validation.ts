import Joi from "joi";
import { ValidateSerialToken } from "../Auth/model";
import Validation from "../validation";
import { IDeviceReadings } from "./model";

/**
 * @export
 * @class DeviceReadingValidation
 * @extends Validation
 */
class DeviceReadingValidation extends Validation {
  /**
   * Creates an instance of DeviceReadingValidation
   * @memberof DeviceReadingValidation
   */
  constructor() {
    super();
  }
  validateReading(params: IDeviceReadings) {
    const schema: Joi.Schema = Joi.object().keys({
      temperature: Joi.number().precision(2).greater(-30.0).less(100.0),
      humidity: Joi.number().precision(2).greater(0.0).less(100.0),
      carbon_monoxide: Joi.number().precision(2).greater(0.0).less(10_00.0),
    });

    return schema.validate(params);
  }
}

export default new DeviceReadingValidation();
