import { Document, Schema } from "mongoose";
import { configuredDatabase } from "../../config/connections/connections";
import { SensorReadings } from "./interface";

/**
 * @export
 * @interface IDeviceReadings
 * @extends {Document}
 */
export interface IDeviceReadings extends Document {
  serialNumber: string;
  snapshot: SensorReadings;
}

const DeviceReadingsSchema: Schema = new Schema(
  {
    serialNumber: {
      type: String,
    },
    snapshot: {
      deviceTime: { type: Date },
      serverTime: { type: Date },
      healthStatus: { type: Object },
      temperature: { type: Number },
      humidity: { type: Number },
      carbon_monoxide: { type: Number },
    },
  },
  { collection: "device" }
);

const DeviceReadingModel = configuredDatabase.model<IDeviceReadings>(
  "DeviceReadingModel",
  DeviceReadingsSchema
);

export default DeviceReadingModel;
