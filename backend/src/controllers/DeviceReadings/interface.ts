import { ValidateSerialToken } from "../Auth/model";
import { IDeviceReadings } from "./model";

enum HealthStatus {
  "Ok",
  "needs_filter",
  "needs_service",
}

export type SensorReadings = {
  deviceTime: Date;
  serverTime: Date;
  healthStatus: keyof HealthStatus;
  temperature: number;
  humidity: number;
  carbon_monoxide: number;
};

/**
 * @export
 * @interface IDeviceReadingService
 */
export interface IDeviceReadingSerivce {
  /**
   * @param {IDeviceReadings}
   * @returns {Promise<void>}
   * @memberof DeviceReadingService
   */
  createSnapshot(data: IDeviceReadings): Promise<void>;
}
