import mongoose from "mongoose";

const uri = process.env.DB_URI;
if (uri === undefined) {
  throw new Error(".env is not configured");
}
export const configuredDatabase = mongoose.createConnection(uri);

configuredDatabase.on("connected", () => {
  console.log("Connected to MongoDB instance");
});

configuredDatabase.on("error", (error) => {
  console.error(`MongoDB connection: ${error}`);
});
