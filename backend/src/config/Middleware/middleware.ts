import winston from "winston";
import expressWinston from "express-winston";
import expressJSDocSwagger from "express-jsdoc-swagger";
import cors from "cors";
import debug from "debug";
import * as routes from "../../routes/index";
import express from "express";
import path from "path/posix";
const debugLog: debug.IDebugger = debug("app");

export default function middleWareInit(app: express.Application) {
  /* Json response */
  app.use(express.json());
  /* Cors enabled */
  app.use(cors());

  const loggerOptions: expressWinston.LoggerOptions = {
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.json(),
      winston.format.prettyPrint(),
      winston.format.colorize({ all: true })
    ),
  };

  const swaggerOptions = {
    info: {
      version: "1.0.0",
      title: "Smart AC",
      license: {
        name: "MIT",
      },
    },
    baseDir: `${path.resolve("")}/dist/src/routes/`,
    // Glob pattern to find your jsdoc files (multiple patterns can be added in an array)
    filesPattern: "./**/*.js",
    // URL where SwaggerUI will be rendered
    swaggerUIPath: "/api-docs",
    // Expose OpenAPI UI
    exposeSwaggerUI: true,
    // Expose Open API JSON Docs documentation in `apiDocsPath` path.
    exposeApiDocs: false,
    // Open API JSON Docs endpoint.
    apiDocsPath: "/v3/api-docs",
    // Set non-required fields as nullable by default
    notRequiredAsNullable: false,
    swaggerUiOptions: {},
    // multiple option in case you want more that one instance
    multiple: false,
  };

  if (process.env.NODE_ENV !== "development") {
    loggerOptions.meta = false;
  }

  expressJSDocSwagger(app)(swaggerOptions);
  app.use(expressWinston.logger(loggerOptions));

  routes.initRoutes(app);
}
