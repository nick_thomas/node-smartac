import express from "express";
import http from "http";
import middleWareInit from "./src/config/Middleware/middleware";
const app: express.Application = express();
const server: http.Server = http.createServer(app);
middleWareInit(app);
const port = 80;
const runningMessage = `Server running at http://localhost:3000 \n Check API docs at /api-docs`;

server.listen(port, () => {
  console.log(runningMessage);
});
